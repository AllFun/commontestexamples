package com.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.edu.infrastructure.persistence.service",
        "com.edu.domain.service",
        "com.edu.infrastructure.persistence.testdata"
}
)
public class MainApp {
    public static void main(final String[] args) {
        SpringApplication.run(MainApp.class, args);
    }
}
