package com.edu.domain.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Builder
@Getter
@RequiredArgsConstructor
public class ValueObject {
    private final String value;
    private final ValueType valueType;
}
