package com.edu.domain.service;

import com.edu.domain.dto.ValueObject;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TestCaseService {

    private final JdbcTemplate jdbcTemplate;

    public <T> List<T> execute(final ValueObject forExecute) {
        return createQuery(forExecute);
    }

    private <T> List<T> createQuery(final ValueObject forExecute) {
        switch (forExecute.getValueType()) {
//            case SELECT_ONE:
//                singletonList(jdbcTemplate.query(forExecute, new AbstractRowMapper<T>()));
            case SELECT:
                return (List<T>) jdbcTemplate.queryForList(forExecute.getValue());
            default:
                throw new UnsupportedOperationException(forExecute.getValueType().name());
        }
    }
}
