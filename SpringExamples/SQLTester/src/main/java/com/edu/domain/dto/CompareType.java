package com.edu.domain.dto;

public enum CompareType {
    EQUALS, REGEX, GREATER, LOWER
}
