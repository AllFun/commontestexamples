package com.edu.domain.dto;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class TestCase {
    private final ValueObject forExecute;
    private final List<ValueObject> expectedResults;
    private final CompareType compareType;
}
