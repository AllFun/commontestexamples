package com.edu.domain.dto;

public enum ValueType {
    SELECT_ONE,
    SELECT,
    INSERT,
    OBJECT,
    CSV,
    EXCEPTION
}
