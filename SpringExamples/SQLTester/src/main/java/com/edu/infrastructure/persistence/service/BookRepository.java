package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BookRepository extends JpaRepository<Book, UUID> {
}
