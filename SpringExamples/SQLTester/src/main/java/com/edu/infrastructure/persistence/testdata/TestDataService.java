package com.edu.infrastructure.persistence.testdata;

import com.edu.infrastructure.persistence.domain.Author;
import com.edu.infrastructure.persistence.domain.Book;
import com.edu.infrastructure.persistence.service.AuthorRepository;
import com.edu.infrastructure.persistence.service.BookRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static com.edu.infrastructure.persistence.testdata.TestDataUtils.HAMLET;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.JULIUS;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.MUCH_ADO_ABOUT_NOTHING;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.NIGHTS_DREAME;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.OTHELLO;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.ROMEO_AND_JULIET;
import static com.edu.infrastructure.persistence.testdata.TestDataUtils.SHAKESPEARE;

@Service
@RequiredArgsConstructor
public class TestDataService {
    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;

    public void insertTestData() {
        getTestAuthors().forEach(authorRepository::saveAndFlush);
        getTestBooks().forEach(bookRepository::saveAndFlush);
//        authorRepository.saveAll(getTestAuthors());
//        authorRepository.flush();
//        bookRepository.saveAll(getTestBooks());
//        bookRepository.flush();
    }

    private static List<Author> getTestAuthors() {
        return Arrays.asList(
                SHAKESPEARE
        );
    }

    private static List<Book> getTestBooks() {
        return Arrays.asList(
                HAMLET, OTHELLO, NIGHTS_DREAME, ROMEO_AND_JULIET, JULIUS, MUCH_ADO_ABOUT_NOTHING
        );
    }
}
