package com.edu.infrastructure.persistence.testdata;

import com.edu.infrastructure.persistence.domain.Author;
import com.edu.infrastructure.persistence.domain.Book;
import lombok.NoArgsConstructor;

import java.util.UUID;

import static java.util.Collections.singletonList;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class TestDataUtils {
    public static final Author SHAKESPEARE = Author.builder()
            .id(UUID.randomUUID())
            .name("William SHAKESPEARE")
            .pseudonym("Bard of Avon")
            .build();

    public static final Book HAMLET = Book.builder()
            .id(UUID.randomUUID())
            .title("The Tragicall Historie of Hamlet, Prince of Denmark")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book OTHELLO = Book.builder()
            .id(UUID.randomUUID())
            .title("The Tragedy of Othello, The Moor of Venice")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book NIGHTS_DREAME = Book.builder()
            .id(UUID.randomUUID())
            .title("A Midsommer nights dreame")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book ROMEO_AND_JULIET = Book.builder()
            .id(UUID.randomUUID())
            .title("An Excellent conceited Tragedy of Romeo and Juliet")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book JULIUS = Book.builder()
            .id(UUID.randomUUID())
            .title("The Tragedie of Julius Cæsar")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book MUCH_ADO_ABOUT_NOTHING = Book.builder()
            .id(UUID.randomUUID())
            .title("Much Ado About Nothing")
            .authors(singletonList(SHAKESPEARE))
            .build();
}
