package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AuthorRepository extends JpaRepository<Author, UUID> {
}
