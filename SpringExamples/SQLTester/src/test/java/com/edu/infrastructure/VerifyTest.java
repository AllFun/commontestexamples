package com.edu.infrastructure;

import com.edu.domain.dto.TestCase;
import com.edu.domain.dto.ValueObject;
import com.edu.domain.service.TestCaseService;
import com.edu.infrastructure.persistence.testdata.TestDataService;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.stream.Stream;

import static com.edu.domain.dto.CompareType.REGEX;
import static com.edu.domain.dto.ValueType.SELECT;
import static java.util.Arrays.asList;

@SpringBootTest
@NoArgsConstructor
public class VerifyTest {

    @Autowired
    private TestCaseService service;

    @Autowired
    private TestDataService testDataService;

    @BeforeEach
    void initTestData() {
        testDataService.insertTestData();
    }

    @MethodSource("queries")
    @ParameterizedTest
    void shouldPassAllQueries(final TestCase testCase) {
        final List<Object> objects = service.execute(testCase.getForExecute());
    }

    private static Stream<Arguments> queries() {
        return Stream.of(
                Arguments.of(TestCase.builder()
                        .forExecute(ValueObject.builder()
                                .value("SELECT sysdate FROM dual;")
                                .valueType(SELECT)
                                .build())
                        .compareType(REGEX)
                        .expectedResults(asList(
                                ValueObject.builder()
                                        .build()
                        ))
                        .build())
        );
    }
}
