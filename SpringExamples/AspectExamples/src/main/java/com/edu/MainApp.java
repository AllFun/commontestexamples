package com.edu;

import com.edu.service.TestService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.util.Arrays;

@Slf4j
@SpringBootApplication
public class MainApp {

    @Autowired
    private TestService testService1;
    @Autowired
    private TestService testService2;

    public static void main(String[] args) {
//        new SpringApplicationBuilder(TestService.class)
//                .web(WebApplicationType.NONE)
//                .run(args);
        SpringApplication.run(MainApp.class, args);
    }


    @EventListener(classes = ContextRefreshedEvent.class)
    void afterStart() {
        System.out.println();
        log.info("testService1");
        testService1.start(Arrays.asList("test value 1", "test value 2"));
        System.out.println();
        log.info("testService2");
        testService2.start(Arrays.asList("test value 1", "test value 2"));
        System.out.println();
    }
}
