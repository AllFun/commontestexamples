package com.edu.config;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * https://habr.com/ru/post/428548/
 */

@Slf4j
@Configuration
@Aspect
public class InterceptorsConfig {

    @Before("execution(public void com.edu.service.TestService.start(..)) && bean(testService1)")
    public void beforeCallAt() {
        log.info("~ 1 beforeCallAt - public void com.edu.service.TestService.start(..)) && bean(testService1)");
    }

    @After("execution(public * com.edu.service.TestService.start(..)) && bean(testService1)")
    public void afterCallAt() {
        log.info("~ 2 afterCallAt - execution(public * com.edu.service.TestService.start(..)) && bean(testService1)");
    }

    @SneakyThrows
    @Around("execution( void com.edu.service.TestService.start(..)) && bean(testService1)")
    public void callAtMyServiceMethod(final ProceedingJoinPoint pjp) {
        log.info("~ 3.1 callAtMyServiceMethod - execution( void com.edu.service.TestService.start(..))) && bean(testService1)");
        pjp.proceed();
        log.info("~ 3.2 callAtMyServiceMethod - execution( void com.edu.service.TestService.start(..))) && bean(testService1)");
    }

    @SneakyThrows
    @Around("execution(* com.edu.service.TestService.start(..)) && args(list,..) && bean(testService2)")
    public void callAtMyServiceMethod1(final ProceedingJoinPoint pjp, final List<String> list) {
        log.info("~ 4.1 callAtMyServiceMethod1: {}", String.join(",", list));
        pjp.proceed();
        log.info("~ 4.2 callAtMyServiceMethod1: {}", String.join(",", list));
    }
}
