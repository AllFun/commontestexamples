package com.edu.config;

import com.edu.service.TestService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfig {
    @Bean
    public TestService testService1() {
        return new TestService();
    }

    @Bean
    public TestService testService2() {
        return new TestService();
    }
}
