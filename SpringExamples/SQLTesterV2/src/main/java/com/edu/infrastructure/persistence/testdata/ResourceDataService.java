package com.edu.infrastructure.persistence.testdata;

import com.edu.infrastructure.persistence.domain.testcase.TestCase;
import lombok.SneakyThrows;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.stream.Stream;

import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.io.FilenameUtils.getBaseName;

@Service
public class ResourceDataService {

    private static final Resource resourceFile = new ClassPathResource("queries");

    @SneakyThrows
    public static Map<String, List<TestCase>> readResources() {
        final BinaryOperator<List<TestCase>> binaryOperator = (l1, l2) -> {
            final ArrayList<TestCase> list = new ArrayList<>();
            list.addAll(l1);
            list.addAll(l2);
            return list;
        };

        return Stream.of(resourceFile.getFile().listFiles())
                .map(TestCase::of)
                .collect(toMap(
                        file -> getBaseName(file.getFile().getName()),
                        file -> new ArrayList<>(singleton(file)),
                        binaryOperator));
    }
}
