package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book, Long> {
}
