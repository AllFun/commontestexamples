package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.entity.Author;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorRepository extends JpaRepository<Author, Long> {
}
