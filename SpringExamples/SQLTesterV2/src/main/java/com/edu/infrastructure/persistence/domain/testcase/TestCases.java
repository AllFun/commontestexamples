package com.edu.infrastructure.persistence.domain.testcase;

import lombok.RequiredArgsConstructor;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
public class TestCases {
    private final String testCaseName;
    private final Map<TestCaseType, TestCase> map;

    public static TestCases of(final List<TestCase> testCases) {
        final HashMap<TestCaseType, TestCase> map = new HashMap<>();
        testCases.forEach(testCase -> map.put(testCase.getType(), testCase));
        final String testCaseName = testCases.stream().findAny()
                .map(TestCase::getFile).map(File::getName).map(FilenameUtils::getBaseName)
                .orElseThrow(IllegalArgumentException::new);

        return new TestCases(testCaseName, map);
    }

    @Override
    public String toString() {
        return testCaseName;
    }
}
