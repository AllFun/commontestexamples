package com.edu.infrastructure.persistence.testdata;

import com.edu.infrastructure.persistence.domain.entity.Author;
import com.edu.infrastructure.persistence.domain.entity.Book;
import lombok.NoArgsConstructor;

import static java.util.Collections.singletonList;
import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public final class TestDataUtils {
    public static final Author SHAKESPEARE = Author.builder()
            .id(1L)
            .name("William SHAKESPEARE")
            .pseudonym("Bard of Avon")
            .build();

    public static final Book HAMLET = Book.builder()
            .id(1L)
            .title("The Tragicall Historie of Hamlet, Prince of Denmark")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book OTHELLO = Book.builder()
            .id(2L)
            .title("The Tragedy of Othello, The Moor of Venice")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book NIGHTS_DREAME = Book.builder()
            .id(3L)
            .title("A Midsommer nights dreame")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book ROMEO_AND_JULIET = Book.builder()
            .id(4L)
            .title("An Excellent conceited Tragedy of Romeo and Juliet")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book JULIUS = Book.builder()
            .id(5L)
            .title("The Tragedie of Julius Cæsar")
            .authors(singletonList(SHAKESPEARE))
            .build();

    public static final Book MUCH_ADO_ABOUT_NOTHING = Book.builder()
            .id(6L)
            .title("Much Ado About Nothing")
            .authors(singletonList(SHAKESPEARE))
            .build();
}
