package com.edu.infrastructure.persistence.domain.testcase;

import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

import static lombok.AccessLevel.PRIVATE;

@RequiredArgsConstructor(access = PRIVATE)
public enum TestCaseType {
    QUERY("sql"),
    CSV("csv"),
    REGEX("rex");

    private final String fileExtension;

    public static TestCaseType fromExtension(final String fileExtension) {
        return Stream.of(TestCaseType.values())
                .filter(type -> type.fileExtension.equals(fileExtension))
                .findAny()
                .orElseThrow(() -> new UnsupportedOperationException(fileExtension));
    }
}
