package com.edu.infrastructure.persistence.domain.testcase;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;

@Getter
@Builder
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class TestCase {

    private final File file;

    private final TestCaseType type;

    private final List<String> content;

    @SneakyThrows
    public static TestCase of(final File file) {
        return TestCase.builder()
                .file(file)
                .type(TestCaseType.fromExtension(FilenameUtils.getExtension(file.getName())))
                .content(FileUtils.readLines(file, Charset.defaultCharset()))
                .build();
    }
}
