package com.edu.infrastructure;

import com.edu.domain.service.TestCaseService;
import com.edu.infrastructure.persistence.domain.testcase.TestCases;
import com.edu.infrastructure.persistence.testdata.ResourceDataService;
import com.edu.infrastructure.persistence.testdata.TestDataService;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Stream;

@SpringBootTest
@NoArgsConstructor
public class VerifyTest {

    @Autowired
    private TestCaseService service;

    @Autowired
    private TestDataService testDataService;

    @BeforeEach
    void initTestData() {
        testDataService.insertTestData();
    }

    @MethodSource("queries")
    @ParameterizedTest
    void shouldPassAllQueries(final TestCases testCases) {
//        final List<Object> objects = service.execute(testCase.getForExecute());
    }

    private static Stream<Arguments> queries() {
        return ResourceDataService.readResources().values().stream().map(TestCases::of).map(Arguments::of);
    }
}
