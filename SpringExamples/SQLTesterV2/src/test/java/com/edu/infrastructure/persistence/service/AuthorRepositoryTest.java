package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.entity.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class AuthorRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @Test
    void shouldPersist() {
        final Author author = authorRepository.saveAndFlush(Author.builder().build());
        final Optional<Author> byId = authorRepository.findById(author.getId());
        assertThat(byId).isPresent().isEqualTo(byId);
    }
}