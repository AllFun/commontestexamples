package com.edu.infrastructure.persistence.service;

import com.edu.infrastructure.persistence.domain.entity.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class BookRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @Test
    void shouldPersist() {
        final Book book = bookRepository.saveAndFlush(Book.builder().build());
        final Optional<Book> byId = bookRepository.findById(book.getId());
        assertThat(byId).isPresent().isEqualTo(byId);
    }
}