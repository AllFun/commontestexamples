package com.edu.infrastructure.persistence.v3.service;

import com.edu.infrastructure.persistence.v3.model.Pen;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PenRepository extends JpaRepository<Pen, UUID> {
}
