package com.edu.infrastructure.persistence.v02.domain;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "t_connection")
@Data
@Builder
@EqualsAndHashCode(of = "id")
@ToString(exclude = {"baseLeft", "baseRight"})
public class Connection {
    @Id
    @GeneratedValue
    private UUID id;

    @JoinColumn(name = "base_left_id")
    @ManyToOne
    private BaseLeft baseLeft;

    @JoinColumn(name = "base_right_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private BaseRight baseRight;
}
