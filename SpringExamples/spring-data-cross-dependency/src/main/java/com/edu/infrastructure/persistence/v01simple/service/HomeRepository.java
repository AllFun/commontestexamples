package com.edu.infrastructure.persistence.v01simple.service;

import com.edu.infrastructure.persistence.v01simple.domain.Home;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface HomeRepository extends JpaRepository<Home, UUID> {
}
