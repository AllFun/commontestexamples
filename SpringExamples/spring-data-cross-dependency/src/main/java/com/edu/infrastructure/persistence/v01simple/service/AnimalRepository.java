package com.edu.infrastructure.persistence.v01simple.service;

import com.edu.infrastructure.persistence.v01simple.domain.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AnimalRepository extends JpaRepository<Animal, UUID> {

}
