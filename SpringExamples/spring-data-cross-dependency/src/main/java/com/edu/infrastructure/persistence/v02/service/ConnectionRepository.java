package com.edu.infrastructure.persistence.v02.service;

import com.edu.infrastructure.persistence.v02.domain.Connection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ConnectionRepository extends JpaRepository<Connection, UUID> {

}
