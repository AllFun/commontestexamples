package com.edu.infrastructure.persistence.v02.service;

import com.edu.infrastructure.persistence.v02.domain.BaseRight;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BaseRightRepository extends JpaRepository<BaseRight, UUID> {
}
