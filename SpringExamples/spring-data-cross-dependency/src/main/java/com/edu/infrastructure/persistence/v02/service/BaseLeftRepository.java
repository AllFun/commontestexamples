package com.edu.infrastructure.persistence.v02.service;

import com.edu.infrastructure.persistence.v02.domain.BaseLeft;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BaseLeftRepository extends JpaRepository<BaseLeft, UUID> {
}
