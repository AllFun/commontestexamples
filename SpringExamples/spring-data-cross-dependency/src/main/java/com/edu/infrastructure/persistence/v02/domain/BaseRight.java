package com.edu.infrastructure.persistence.v02.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "t_base_right")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode(of = "id")
@ToString(exclude = "parent")
public class BaseRight {
    @Id
    @GeneratedValue
    private UUID id;

    @OneToMany(mappedBy = "baseRight", cascade = CascadeType.ALL)
    private Set<Connection> connections;

    @OneToMany(mappedBy = "parent")
    private Set<BaseRight> children;

    @ManyToOne
    @JoinColumn(name = "parent")
    private BaseRight parent;
}
