package com.edu.infrastructure.persistence.v3.service;

import com.edu.infrastructure.persistence.v3.model.PenBox;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PenboxRepository extends JpaRepository<PenBox, UUID> {
}
