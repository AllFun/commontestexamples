package com.edu.infrastructure.persistence.v3.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "t_pen")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(exclude = "penBox")
@ToString(exclude = "penBox")
public class Pen {
    @Id
    private UUID id;

    @Column(name = "pen_box_id")
    private UUID penboxId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pen_box_id", insertable = false, updatable = false)
    private PenBox penBox;
}
