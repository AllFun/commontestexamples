package com.edu.infrastructure.persistence.v03;

import com.edu.infrastructure.persistence.v3.model.Pen;
import com.edu.infrastructure.persistence.v3.model.PenBox;
import com.edu.infrastructure.persistence.v3.service.PenRepository;
import com.edu.infrastructure.persistence.v3.service.PenboxRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private PenRepository penRepository;

    @Autowired
    private PenboxRepository penboxRepository;

    @Autowired
    private EntityManager entityManager;

    @Test
    void shouldPersistAndFetch() {
        final UUID penboxId = UUID.fromString("1bdf09c2-f0bf-4465-a800-1a89a0aac398");
        final UUID penId = UUID.fromString("b697b9d9-d89f-4d62-b275-434d148665c9");
        final PenBox penBox = penboxRepository.save(PenBox.builder().id(penboxId).build());
        final Pen pen = penRepository.save(Pen.builder().id(penId).penboxId(penboxId).build());

        verifyDBRecords(penId, penboxId, null, null);

        // clear cache
        entityManager.clear();

        final Pen expectedPen = Pen.builder().id(penId).penboxId(penboxId).build();
        final PenBox expectedPenBox = PenBox.builder()
                .id(penboxId).pens(new HashSet<>(asList(expectedPen)))
                .build();
        expectedPen.setPenBox(penBox);

        verifyDBRecords(penId, penboxId, singleton(expectedPen), expectedPenBox);
    }

    private void verifyDBRecords(final UUID penId, final UUID penboxId, final Set<Pen> expectedPens, final PenBox expectedPenBox) {
        assertThat(penRepository.findAll())
                .hasSize(1)
                .flatExtracting(Pen::getId, Pen::getPenboxId, Pen::getPenBox)
                .containsExactly(penId, penboxId, expectedPenBox);

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaQuery<Pen> query = criteriaBuilder.createQuery(Pen.class);
        final Root<Pen> root = query.from(Pen.class);
        root.fetch("penBox");
        query.select(root);
        assertThat(entityManager.createQuery(query).getResultList())
                .hasSize(1)
                .flatExtracting(Pen::getId, Pen::getPenboxId, Pen::getPenBox)
                .containsExactly(penId, penboxId, expectedPenBox);


        assertThat(penboxRepository.findAll())
                .hasSize(1)
                .flatExtracting(PenBox::getId, PenBox::getPens)
                .containsExactly(penboxId, expectedPens);

        final CriteriaQuery<PenBox> penBoxCriteriaQuery = criteriaBuilder.createQuery(PenBox.class);
        final Root<PenBox> penBoxRoot = penBoxCriteriaQuery.from(PenBox.class);
        penBoxRoot.fetch("pens");
        penBoxCriteriaQuery.select(penBoxRoot);
        assertThat(entityManager.createQuery(penBoxCriteriaQuery).getResultList())
                .hasSize(1)
                .flatExtracting(PenBox::getId, PenBox::getPens)
                .containsExactly(penboxId, expectedPens);

        // for debug
        log.info("~~{}", StringUtils.collectionToCommaDelimitedString(penboxRepository.findAll()));
        log.info("~~{}", StringUtils.collectionToCommaDelimitedString(penRepository.findAll()));
    }

    @Test
    void cacheRefreshComparison() {
        final UUID penboxId = UUID.fromString("1bdf09c2-f0bf-4465-a800-1a89a0aac398");
        final UUID penId = UUID.fromString("b697b9d9-d89f-4d62-b275-434d148665c9");
        final PenBox penBox = penboxRepository.save(PenBox.builder().id(penboxId).build());
        final Pen pen = penRepository.save(Pen.builder().id(penId).penboxId(penboxId).build());

        final Pen expectedPen = Pen.builder().id(penId).penboxId(penboxId).build();
        final PenBox expectedPenBox = PenBox.builder()
                .id(penboxId).pens(new HashSet<>(asList(expectedPen)))
                .build();
        expectedPen.setPenBox(penBox);

        /////////////////////////////////////////////////////////////////////
        assertThat(penRepository.findAll())
                .hasSize(1)
                .flatExtracting(Pen::getId, Pen::getPenboxId, Pen::getPenBox)
                .usingFieldByFieldElementComparator()
                .containsExactly(penId, penboxId, null);

        entityManager.refresh(pen);

        assertThat(penRepository.findAll())
                .hasSize(1)
                .flatExtracting(Pen::getId, Pen::getPenboxId, Pen::getPenBox)
                .containsExactly(penId, penboxId, expectedPenBox);
        /////////////////////////////////////////////////////////////////////
        assertThat(penboxRepository.findAll())
                .hasSize(1)
                .flatExtracting(PenBox::getId, PenBox::getPens)
                .containsExactly(penboxId, null);

        entityManager.refresh(pen);

        assertThat(penboxRepository.findAll())
                .hasSize(1)
                .flatExtracting(PenBox::getId, PenBox::getPens)
                .containsExactly(penboxId, singleton(expectedPen));
        /////////////////////////////////////////////////////////////////////
    }
}
