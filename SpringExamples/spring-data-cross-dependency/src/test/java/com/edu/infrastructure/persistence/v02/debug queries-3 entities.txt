entityManager.refresh(connection);
entityManager.refresh(baseLeft);
entityManager.refresh(baseRight);

Hibernate:
    select
        baseright0_.id as id1_2_1_,
        baseright0_.parent as parent2_2_1_,
        connection1_.base_right_id as base_rig3_3_3_,
        connection1_.id as id1_3_3_,
        connection1_.id as id1_3_0_,
        connection1_.base_left_id as base_lef2_3_0_,
        connection1_.base_right_id as base_rig3_3_0_
    from
        t_base_right baseright0_
    left outer join
        t_connection connection1_
            on baseright0_.id=connection1_.base_right_id
    where
        baseright0_.id=?
Hibernate:
    select
        connection0_.id as id1_3_1_,
        connection0_.base_left_id as base_lef2_3_1_,
        connection0_.base_right_id as base_rig3_3_1_,
        baseright1_.id as id1_2_0_,
        baseright1_.parent as parent2_2_0_
    from
        t_connection connection0_
    left outer join
        t_base_right baseright1_
            on connection0_.base_right_id=baseright1_.id
    where
        connection0_.id=?
Hibernate:
    select
        baseright0_.id as id1_2_1_,
        baseright0_.parent as parent2_2_1_,
        connection1_.base_right_id as base_rig3_3_3_,
        connection1_.id as id1_3_3_,
        connection1_.id as id1_3_0_,
        connection1_.base_left_id as base_lef2_3_0_,
        connection1_.base_right_id as base_rig3_3_0_
    from
        t_base_right baseright0_
    left outer join
        t_connection connection1_
            on baseright0_.id=connection1_.base_right_id
    where
        baseright0_.id=?
Hibernate:
    select
        connection0_.id as id1_3_1_,
        connection0_.base_left_id as base_lef2_3_1_,
        connection0_.base_right_id as base_rig3_3_1_,
        baseright1_.id as id1_2_0_,
        baseright1_.parent as parent2_2_0_
    from
        t_connection connection0_
    left outer join
        t_base_right baseright1_
            on connection0_.base_right_id=baseright1_.id
    where
        connection0_.id=?
Hibernate:
    select
        baseleft0_.id as id1_1_2_,
        connection1_.base_left_id as base_lef2_3_4_,
        connection1_.id as id1_3_4_,
        connection1_.id as id1_3_0_,
        connection1_.base_left_id as base_lef2_3_0_,
        connection1_.base_right_id as base_rig3_3_0_,
        baseright2_.id as id1_2_1_,
        baseright2_.parent as parent2_2_1_
    from
        t_base_left baseleft0_
    left outer join
        t_connection connection1_
            on baseleft0_.id=connection1_.base_left_id
    left outer join
        t_base_right baseright2_
            on connection1_.base_right_id=baseright2_.id
    where
        baseleft0_.id=?
Hibernate:
    select
        connection0_.id as id1_3_1_,
        connection0_.base_left_id as base_lef2_3_1_,
        connection0_.base_right_id as base_rig3_3_1_,
        baseright1_.id as id1_2_0_,
        baseright1_.parent as parent2_2_0_
    from
        t_connection connection0_
    left outer join
        t_base_right baseright1_
            on connection0_.base_right_id=baseright1_.id
    where
        connection0_.id=?
Hibernate:
    select
        baseright0_.id as id1_2_1_,
        baseright0_.parent as parent2_2_1_,
        connection1_.base_right_id as base_rig3_3_3_,
        connection1_.id as id1_3_3_,
        connection1_.id as id1_3_0_,
        connection1_.base_left_id as base_lef2_3_0_,
        connection1_.base_right_id as base_rig3_3_0_
    from
        t_base_right baseright0_
    left outer join
        t_connection connection1_
            on baseright0_.id=connection1_.base_right_id
    where
        baseright0_.id=?