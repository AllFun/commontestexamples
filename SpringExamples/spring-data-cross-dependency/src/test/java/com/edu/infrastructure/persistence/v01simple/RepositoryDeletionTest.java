package com.edu.infrastructure.persistence.v01simple;

import com.edu.infrastructure.persistence.v01simple.domain.Animal;
import com.edu.infrastructure.persistence.v01simple.domain.Home;
import com.edu.infrastructure.persistence.v01simple.service.AnimalRepository;
import com.edu.infrastructure.persistence.v01simple.service.HomeRepository;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.SoftAssertions.assertSoftly;


@DataJpaTest
public class RepositoryDeletionTest {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private HomeRepository homeRepository;

    @Autowired
    private EntityManager entityManager;

    @BeforeEach
    void clearDB() {
        animalRepository.deleteAll();
    }

    @Test
    void shouldRemoveAll() {
        verifyIsEmptyDB();
        prepareTestData();

        animalRepository.deleteAll();
        homeRepository.deleteAll();

        verifyIsEmptyDB();
    }

    @Test
    void shouldRemoveCascade() {
        verifyIsEmptyDB();
        prepareTestData();

        animalRepository.deleteAll();

        verifyIsEmptyDB();
    }

    @Test
    void shouldRemoveCascadeFromMappedBy() {
        verifyIsEmptyDB();
        prepareTestData();

        homeRepository.deleteAll();

        assertSoftly(
                softAssertions -> {
                    softAssertions.assertThat(homeRepository.findAll()).isNotEmpty();
                    softAssertions.assertThat(animalRepository.findAll()).isNotEmpty();
                }
        );
    }

    @Test
    void shouldFailTryingToRemoveHome() {
        verifyIsEmptyDB();
        prepareTestData();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Home> criteriaDelete = criteriaBuilder.createCriteriaDelete(Home.class);
        assertThatThrownBy(() -> entityManager.createQuery(criteriaDelete))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Error occurred validating the Criteria")
                .hasCauseExactlyInstanceOf(IllegalStateException.class)
                .hasRootCauseMessage("UPDATE/DELETE criteria must name root entity");
    }

    @Test
    void shouldFailTryingToRemoveAnimal() {
        verifyIsEmptyDB();
        prepareTestData();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Animal> criteriaDelete = criteriaBuilder.createCriteriaDelete(Animal.class);
        assertThatThrownBy(() -> entityManager.createQuery(criteriaDelete))
                .isExactlyInstanceOf(IllegalArgumentException.class)
                .hasMessage("Error occurred validating the Criteria")
                .hasCauseExactlyInstanceOf(IllegalStateException.class)
                .hasRootCauseMessage("UPDATE/DELETE criteria must name root entity");
    }

    @Test
    void shouldRemoveAllByAnimal() {
        verifyIsEmptyDB();
        prepareTestData();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Animal> criteriaDelete = criteriaBuilder.createCriteriaDelete(Animal.class);
        final Root<Animal> root = criteriaDelete.from(Animal.class);
        entityManager.createQuery(criteriaDelete).executeUpdate();

        assertSoftly(
                softAssertions -> {
                    softAssertions.assertThat(homeRepository.findAll()).isNotEmpty();
                    softAssertions.assertThat(animalRepository.findAll()).isEmpty();
                }
        );
    }

    @Test
    void shouldRemoveAllByHome() {
        verifyIsEmptyDB();
        prepareTestData();

        final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        final CriteriaDelete<Home> criteriaDelete = criteriaBuilder.createCriteriaDelete(Home.class);
        final Root<Home> root = criteriaDelete.from(Home.class);
        final Query query = entityManager.createQuery(criteriaDelete);
        assertThatThrownBy(() -> query.executeUpdate())
                .isExactlyInstanceOf(PersistenceException.class)
                .hasMessage("org.hibernate.exception.ConstraintViolationException: could not execute statement")
                .getRootCause()
                .isExactlyInstanceOf(JdbcSQLIntegrityConstraintViolationException.class)
                .hasMessageContaining("Referential integrity constraint violation: ");
    }

    private void verifyIsEmptyDB() {
        assertSoftly(
                softAssertions -> {
                    softAssertions.assertThat(homeRepository.findAll()).isEmpty();
                    softAssertions.assertThat(animalRepository.findAll()).isEmpty();
                }
        );
    }

    private void prepareTestData() {
        final Home home = homeRepository.save(Home.builder().build());
        final Animal animal = animalRepository.save(Animal.builder()
                .home(home)
                .build());

        assertThat(animal).isNotNull();
        assertThat(home).isNotNull();

        assertThat(homeRepository.findAll())
                .hasSize(1)
                .extracting(Home::getAnimals)
                .containsNull();

        assertThat(animalRepository.findAll())
                .hasSize(1)
                .extracting(Animal::getHome)
                .containsExactly(home);
    }
}
