package com.edu.infrastructure.persistence.v01simple;

import com.edu.infrastructure.persistence.v01simple.domain.Animal;
import com.edu.infrastructure.persistence.v01simple.domain.Home;
import com.edu.infrastructure.persistence.v01simple.service.AnimalRepository;
import com.edu.infrastructure.persistence.v01simple.service.HomeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
public class RepositoryTest {

    @Autowired
    private AnimalRepository animalRepository;

    @Autowired
    private HomeRepository homeRepository;

    @Test
    void shouldPersistHomeInAnimal() {
        final Home home = homeRepository.save(Home.builder().build());
        final Animal animal = animalRepository.save(Animal.builder()
                .home(home)
                .build());

        assertThat(animal).isNotNull();
        assertThat(home).isNotNull();

        assertThat(homeRepository.findAll())
                .hasSize(1)
                .extracting(Home::getAnimals)
                .containsNull();

        assertThat(animalRepository.findAll())
                .hasSize(1)
                .extracting(Animal::getHome)
                .containsExactly(home);
    }

    @Test
    void shouldPersistBoth() {
        final Home home = homeRepository.save(Home.builder().build());
        final Animal animal = animalRepository.save(Animal.builder()
                .home(home)
                .build());
        home.setAnimals(asList(animal));

        assertThat(animal).isNotNull();
        assertThat(home).isNotNull();

        final Home one = homeRepository.getOne(home.getId());
        assertThat(one).isNotNull();
        assertThat(one.getAnimals())
                .contains(animal);

        assertThat(homeRepository.findAll())
                .hasSize(1)
                .flatExtracting(Home::getAnimals)
                .containsExactly(animal);

        assertThat(animalRepository.findAll())
                .hasSize(1)
                .extracting(Animal::getHome)
                .containsExactly(home);
    }

    @Test
    void shouldPersistBothInverse() {
        final Animal animal = animalRepository.save(Animal.builder().build());
        final Home home = homeRepository.save(Home.builder()
                .animals(asList(animal))
                .build());
        animal.setHome(home);

        assertThat(animal).isNotNull();
        assertThat(home).isNotNull();

        final Home one = homeRepository.getOne(home.getId());
        assertThat(one).isNotNull();
        assertThat(one.getAnimals())
                .contains(animal);

        assertThat(homeRepository.findAll())
                .hasSize(1)
                .flatExtracting(Home::getAnimals)
                .containsExactly(animal);

        assertThat(animalRepository.findAll())
                .hasSize(1)
                .extracting(Animal::getHome)
                .containsExactly(home);
    }

    @Test
    void shouldPersistAnimalInHome() {
        final Animal animal = animalRepository.save(Animal.builder().build());
        final Home home = homeRepository.save(Home.builder()
                .animals(asList(animal))
                .build());

        assertThat(animal).isNotNull();
        assertThat(home).isNotNull();

        final Home one = homeRepository.getOne(home.getId());
        assertThat(one).isNotNull();
        assertThat(one.getAnimals())
                .contains(animal);

        assertThat(homeRepository.findAll())
                .hasSize(1)
                .flatExtracting(Home::getAnimals)
                .containsExactly(animal);

        assertThat(animalRepository.findAll())
                .hasSize(1)
                .extracting(Animal::getHome)
                .containsNull();
    }
}
