package com.edu.infrastructure.persistence.v02;

import com.edu.infrastructure.persistence.v02.domain.BaseLeft;
import com.edu.infrastructure.persistence.v02.domain.BaseRight;
import com.edu.infrastructure.persistence.v02.domain.Connection;
import com.edu.infrastructure.persistence.v02.service.BaseLeftRepository;
import com.edu.infrastructure.persistence.v02.service.BaseRightRepository;
import com.edu.infrastructure.persistence.v02.service.ConnectionRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.persistence.EntityManager;
import java.util.HashSet;

import static java.util.Arrays.asList;
import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@DataJpaTest
public class RepositoryTest {

    @Autowired
    private BaseLeftRepository leftRepository;
    @Autowired
    private BaseRightRepository rightRepository;
    @Autowired
    private ConnectionRepository connectionRepository;
    @Autowired
    private EntityManager entityManager;

    @Test
    void shouldPersistAll() {
        final BaseLeft baseLeft = leftRepository.save(BaseLeft.builder().build());
        final BaseRight baseRight = rightRepository.save(BaseRight.builder().build());
        final Connection connection = connectionRepository.save(Connection.builder()
                .baseLeft(baseLeft)
                .baseRight(baseRight)
                .build());
        baseLeft.setConnections(singleton(connection));
        baseRight.setConnections(singleton(connection));

        verifyForShouldPersistAll(connection, baseLeft, baseRight);

//        entityManager.refresh(connection);
        entityManager.refresh(baseLeft);
        entityManager.refresh(baseRight);

        verifyForShouldPersistAll(connection, baseLeft, baseRight);
    }

    private void verifyForShouldPersistAll(final Connection connection,
                                           final BaseLeft baseLeft,
                                           final BaseRight baseRight) {
        log.info("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        assertThat(connectionRepository.findAll())
                .hasSize(1)
                .contains(connection)
                .flatExtracting(Connection::getBaseLeft, Connection::getBaseRight)
                .contains(baseLeft, baseRight);

        assertThat(leftRepository.findAll())
                .hasSize(1)
                .containsExactly(baseLeft)
                .flatExtracting(BaseLeft::getConnections)
                .contains(connection);

        assertThat(rightRepository.findAll())
                .hasSize(1)
                .containsExactly(baseRight)
                .flatExtracting(BaseRight::getConnections)
                .contains(connection);
        log.info("///////////////////////////////////////////////////////////////");
    }

    @Test
    void shouldPersistAllAdvanced() {
        final BaseLeft baseLeft = leftRepository.save(BaseLeft.builder().build());
        final BaseRight baseRight = rightRepository.save(BaseRight.builder().build());
        final Connection connection = connectionRepository.save(Connection.builder()
                .baseLeft(baseLeft)
                .baseRight(baseRight)
                .build());
        baseLeft.setConnections(singleton(connection));
        baseRight.setConnections(singleton(connection));

        final BaseRight chld1 = rightRepository.save(BaseRight.builder().build());
        final BaseRight chld2 = rightRepository.save(BaseRight.builder().build());

        baseRight.setChildren(new HashSet<>(asList(chld1, chld2)));

        assertThat(connectionRepository.findAll())
                .hasSize(1)
                .contains(connection)
                .flatExtracting(Connection::getBaseLeft, Connection::getBaseRight)
                .contains(baseLeft, baseRight);

        assertThat(leftRepository.findAll())
                .hasSize(1)
                .containsExactly(baseLeft)
                .flatExtracting(BaseLeft::getConnections)
                .contains(connection);

        assertThat(rightRepository.findAll())
                .hasSize(3)
                .containsExactly(baseRight, chld1, chld2);
    }
}
