package com.edu.arrays;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class MultiDimArrays {
    @Test
    void initArray() {
        int size = 5;
        int[][] array = new int[size][];
        for (int i = 0; i < 5; i++) {
            array[i] = new int[i + 1];
        }
        int count = 0;
        for (int[] i : array) {
            for (int j : i) {
                System.out.print(j + " ");
                count++;
            }
            System.out.println();
        }

        assertThat(count).isEqualTo(5 + 4 + 3 + 2 + 1);
    }
}
