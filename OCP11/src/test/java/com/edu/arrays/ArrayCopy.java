package com.edu.arrays;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class ArrayCopy {
    private static final int[] source = {1, 2, 3, 4, 5};

    @Test
    void copyArray() {
        int[] destination = new int[3];
        System.arraycopy(source, 1, destination, 0, destination.length);

        assertThat(destination).containsExactly(2, 3, 4);
    }

    @Test
    void arraysCopy() {
        assertThat(Arrays.copyOf(source, source.length)).containsExactly(1, 2, 3, 4, 5);
        assertThat(Arrays.copyOf(source, source.length - 2)).containsExactly(1, 2, 3);
    }

    @Test
    void arraysCopyOfRange() {
        assertThat(Arrays.copyOfRange(source, 1, source.length - 1)).containsExactly(2, 3, 4);
    }
}
