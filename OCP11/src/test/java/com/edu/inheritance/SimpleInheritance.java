package com.edu.inheritance;

import lombok.Getter;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SimpleInheritance {

    @Test
    void shouldVerifyInheritance() {
        final Drink drink = new Drink();
        assertThat(drink.toString()).startsWith("com.edu.inheritance.Drink@");
        assertThat(((Product) drink).toString()).startsWith("com.edu.inheritance.Drink@");
        assertThat(((Object) drink).toString()).startsWith("com.edu.inheritance.Drink@");

        assertThat(drink.toString()).startsWith("com.edu.inheritance.Drink@");
        assertThat(drink.invokeParentToString()).startsWith("com.edu.inheritance.Drink@");

        assertThat(drink.getName()).isEqualTo("Drink");
        assertThat(drink.getParentName()).isEqualTo("Product");
    }

}

@Getter
class Product {
    private final String name = "Product";
}

@Getter
class Drink extends Product {
    private final String name = "Drink";

    public String getParentName() {
        return super.getName();
    }

    public String invokeParentToString() {
        return super.toString();
    }
}