package com.edu.inheritance;

import lombok.Getter;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.params.provider.Arguments.arguments;

class SwitchForInheritance {

    @ParameterizedTest
    @MethodSource("getArgs")
    void shouldProcess(final Building building) {
//        switch (building.getName()) {
//            case Building.name:
//
//        }
        //TODO finish example
    }


    private static final Stream<Arguments> getArgs() {
        return Stream.of(
                arguments(new Home())
        );
    }
}

@Getter
class Building {
    public static final String name = Building.class.getSimpleName();

    public String getName() {
        return name;
    }
}

@Getter
class Home extends Building {
    private static final String name = Building.class.getSimpleName();
}

@Getter
class Apart extends Building {
    private static final String name = Building.class.getSimpleName();
}