package com.edu;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CastPracticeTest {
    @Test
    void test001() {
        byte b;
        short s;
        int i;
        long l;
        float f;
        double d;
        char c;

        b = -128;
        s = b;
        i = s;
        l = i;
        f = l;
        d = f;
        c = (char) d;
        assertThat(d).isEqualTo(b).isEqualTo(-128).isEqualTo((byte) c);
    }

    @Test
    void test002() {
        byte b;
        short s;
        int i;
        long l;
        float f;
        double d;
        char c;

        d = -128;
        f = (float) d;
        l = (long) f;
        i = (int) l;
        s = (short) i;
        b = (byte) s;
        assertThat(d).isEqualTo(b).isEqualTo(-128);
    }

    @Test
    void test003() {
        assertThat(((int) 5.0 / 2)).isEqualTo(2);

        short s = 3;
        s += 4.6;
        assertThat(s).isEqualTo(7);
    }

    @Test
    void test004() {
        byte starting = 3;
        short firstValue = 5;
        int secondValue = 7;
        int functionValue = (int) (starting / 2 + firstValue / 2 + (int) firstValue / 3) + secondValue / 2;
        assertThat(functionValue).isEqualTo(7);
    }

    @Test
    void test005() {
        int i = 1234567890;
        float f = i;
        assertThat(i - (int) f)
                .isNotEqualTo(0)
                .isEqualTo(-46);

        assertThat(i - f)
                .isEqualTo(0);
    }
}
