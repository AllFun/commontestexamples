package com.edu.mock;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.data.Index.atIndex;

public class CollectionsTest {

    @Test
    void test01() {
        int[][] matrix = new int[5][4];
        for (var i = 0; i < 5; i++)
            matrix[i][1] = 1;
        assertThat(matrix)
                .contains(new int[]{0, 1, 0, 0,}, atIndex(0))
                .contains(new int[]{0, 1, 0, 0,}, atIndex(0))
                .contains(new int[]{0, 1, 0, 0,}, atIndex(0))
                .contains(new int[]{0, 1, 0, 0,}, atIndex(0))
                .contains(new int[]{0, 1, 0, 0,}, atIndex(0));
    }

    @Test
    void test02() {
        var i = 0;
        int[] iA = {10, 20};
        iA[i] = i = 30;
        assertThat(iA).containsExactly(30, 20);
        assertThat(i).isEqualTo(30);
    }

    @Test
    void test03() {
        var i = 0;
        int[] iA = {0, 0};
        iA[++i] = i += 3 + i++;
        assertThat(iA).containsExactly(0, 5);
        assertThat(i).isEqualTo(5);
    }

    @Test
    void test04() {
        var i = 0;
        i += i += i += i += i++;
        assertThat(i).isEqualTo(0);
    }

    @Test
    void test05() {
        var i = 0;
        i += i += i += i += ++i;
        assertThat(i).isEqualTo(1);
    }

    @Test
    void test06() {
        var i = 1;
        i += i += i += i += ++i;
        assertThat(i).isEqualTo(6);
    }

    @Test
    void test07() {
        var i = 1;
        i += i += i += i += i += 5;
        assertThat(i).isEqualTo(10);
    }

    @Test
    void test08() {
        final ArrayList<Double> al = new ArrayList<>();
        assertThat(al.add((double) 111)).isTrue();
        assertThat(al.indexOf(1.0)).isEqualTo(-1);
        assertThat(al.contains("string")).isFalse();
    }

    @Test
    void test09() {
        Deque<Integer> d = new ArrayDeque<>();
        d.push(1);
        assertThat(d.offerLast(2)).isTrue();
        d.push(3);
        assertThat(d).containsExactly(3, 1, 2);
        assertThat(d.peekFirst()).isEqualTo(3);
        assertThat(d.removeLast()).isEqualTo(2);
        assertThat(d.pop()).isEqualTo(3);
        assertThat(d).containsExactly(1);
    }


    @Test
    void test10() {
        @RequiredArgsConstructor
        class Book {
            private final int id;
        }
        final List<Book> books = Arrays.asList(new Book(1), new Book(2));
        Assertions.assertThatThrownBy(() -> books.stream().sorted().findAny())
                .isExactlyInstanceOf(ClassCastException.class)
                .hasMessageContaining("Book cannot be cast to class java.lang.Comparable");
    }

    @Test
    void test11() {
        @RequiredArgsConstructor
        class Book {
            private final int id;
        }
        final Book book = new Book(1);
        final List<Book> books = Arrays.asList(book);
        assertThat(books.stream().sorted().findAny())
                .isPresent().contains(book);
    }

    @Test
    void test12() {
        class MyStringComparator implements Comparator {
            public int compare(Object o1, Object o2) {
                int s1 = ((String) o1).length();
                int s2 = ((String) o2).length();
                return s1 - s2;
            }
        }
        String[] sa = {"d", "bbb", "aaaa"};

        //Since there is no string of length 2 in sa, nothing in sa matches the string "cc".
        // So the return value has to be negative. Further, if the value "cc" were to be inserted in sa,
        // it would have to be inserted after "d" i.e. at index 1. Thus, the return value will be -(index+1) = -2.
        assertThat(Arrays.binarySearch(sa, "cc", new MyStringComparator())).isEqualTo(-2);

        //There is only one string of length 1 in sa, and it is at index 0.
        assertThat(Arrays.binarySearch(sa, "c", new MyStringComparator())).isEqualTo(0);
        assertThat(Arrays.binarySearch(sa, "e", new MyStringComparator())).isEqualTo(0);
        assertThat(Arrays.binarySearch(sa, "", new MyStringComparator())).isEqualTo(-1);

        assertThat(Arrays.binarySearch(sa, "aaaa", new MyStringComparator())).isEqualTo(2);
    }

    @Test
    void test13() {
        int[] arr = {0, 1, 2, 3, 4, 5, 6, 7};
        assertThat(Arrays.binarySearch(arr, -1)).isEqualTo(-1);
        assertThat(Arrays.binarySearch(arr, 0)).isEqualTo(0);
        assertThat(Arrays.binarySearch(arr, 1)).isEqualTo(1);
        assertThat(Arrays.binarySearch(arr, 2)).isEqualTo(2);
        assertThat(Arrays.binarySearch(arr, 3)).isEqualTo(3);
        assertThat(Arrays.binarySearch(arr, -2)).isEqualTo(-1);
        assertThat(Arrays.binarySearch(arr, -20)).isEqualTo(-1);
        assertThat(Arrays.binarySearch(arr, 20)).isEqualTo(-9);
    }

    @Test
    void test14() {
        @AllArgsConstructor
        class Address implements Comparable<Address> {
            final String street;
            final String zip;

            public int compareTo(Address o) {
                int x = this.zip.compareTo(o.zip);
                return x == 0 ? this.street.compareTo(o.street) : x;
            }
        }
        final Address add1 = new Address("dupont dr", "28217");
        final Address add2 = new Address("sharview cir", "28217");
        final Address add3 = new Address("yorkmont ridge ln", "11223");
        ArrayList<Address> al = new ArrayList<>();
        al.add(add1);
        al.add(add2);
        al.add(add3);
        Collections.sort(al);

        assertThat(al).containsExactly(add3, add1, add2);
    }

    @Test
    void test15() {
        @Getter
        @RequiredArgsConstructor
        class Book {
            final String title;
            final String genre;
        }
        final Book b1 = new Book("The Outsider", "fiction");
        final Book b2 = new Book("Becoming", "non-fiction");
        final Book b3 = new Book("Uri", "non-fiction");
        var books = new ArrayList<Book>(List.of(b1, b2, b3));

        books.sort(Comparator.comparing(Book::getGenre).thenComparing(Book::getTitle).reversed());

        assertThat(books).containsExactly(b3, b2, b1);
    }

    @Test
    void test16() {
        class AX {
            int[] x = new int[0];

            {
                x[0] = 10;
            }
        }
        assertThatThrownBy(() -> new AX())
                .isExactlyInstanceOf(ArrayIndexOutOfBoundsException.class)
                .hasMessageContaining("Index 0 out of bounds for length 0");
    }

    static class AX {
        static int[] x = new int[0];

        static {
            x[0] = 10;
        }
    }

    @Test
    void test17() {
        assertThatThrownBy(() -> new AX())
                .isExactlyInstanceOf(ExceptionInInitializerError.class)
                .hasMessage(null);
    }

    @Test
    void test18() {
        char[] a = {'h', 'e', 'l', 'l'};
        char[] b = {};
        assertThat(Arrays.compare(a, b)).isEqualTo(4);
        assertThat(Arrays.mismatch(a, b)).isEqualTo(0);
    }

    @Test
    void test19() {
        Deque<Integer> d = new ArrayDeque<>();
        d.add(1);
        d.add(2);
        d.addFirst(3);
        assertThat(d.pollLast()).isEqualTo(2);
        assertThat(d.pollLast()).isEqualTo(1);
        assertThat(d.pollLast()).isEqualTo(3);
    }

    @Test
    void test20() {
        Deque<Integer> d = new ArrayDeque<>();
        d.add(1);
        d.add(2);
        d.add(3);
        d.add(4);
        assertThat(d.peek()).isEqualTo(1);
        assertThat(d.pollLast()).isEqualTo(4);
        assertThat(d.pollFirst()).isEqualTo(1);
        assertThat(d.poll()).isEqualTo(2);
    }

    @Test
    void test21() {
        Queue<Integer> d = new PriorityQueue<>();
        d.add(1);
        d.add(2);
        d.add(3);
        d.add(4);
        assertThat(d.peek()).isEqualTo(1);
        assertThat(d.poll()).isEqualTo(1);
        assertThat(d.remove()).isEqualTo(2);
    }

    @Test
    void test22() {
        class Booby {
        }
        class Dooby extends Booby {
        }
        class Tooby extends Dooby {
        }
        List<? super Booby> bV = new ArrayList<>();
        List<? extends Tooby> tV = Arrays.asList(new Tooby());

        bV.add(tV.get(0));
    }
}
