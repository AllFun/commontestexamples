package com.edu.mock;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class ExceptionsTest {

    @Test
    void test1() {
        class NewException extends Exception {
        }
        class AnotherException extends Exception {
        }
        class ExceptionTest {
            public void main(String[] args) throws Exception {
                try {
                    m2();
                } finally {
                    m3();
                }
            }

            public void m2() throws NewException {
                throw new NewException();
            }

            public void m3() throws AnotherException {
                throw new AnotherException();
            }
        }
        assertThatThrownBy(() -> new ExceptionTest().main(null))
                .isExactlyInstanceOf(AnotherException.class);
    }

    @Test
    void test02() {
        class TestClass {
            float parseFloat(String s) {
                float f = 0.0f;
                try {
                    f = Float.valueOf(s).floatValue();
                    return f;
                } catch (NumberFormatException nfe) {
                    f = Float.NaN;
                    return f;
                } finally {
                    f = 10.0f;
                    return f;
                }
            }
        }
        assertThat(new TestClass().parseFloat("0.0")).isEqualTo(10F);
    }

    @Test
    void test03() {
        class Test {
            int i1, i2, i3;

            public String main() {
                try {
                    test(i1 = 1, oops(i2 = 2), i3 = 3);
                } catch (Exception e) {
                    return i1 + " " + i2 + " " + i3;
                }
                return null;
            }

            int oops(int i) throws Exception {
                throw new Exception("oops");
            }

            int test(int a, int b, int c) {
                return a + b + c;
            }
        }
        assertThat(new Test().main()).isEqualTo("1 2 0");
    }

    @Test
    void test04() {
        class TestClass {
            public void main() throws Exception {
                Exception e = null;
                throw e;
            }
        }
        assertThatThrownBy(() -> new TestClass().main())
                .isExactlyInstanceOf(NullPointerException.class);
    }

    @Test
    void test05() {
        class Test {
            public Integer main() {
                int j = 1;
                try {
                    int i = doIt() / (j = 2);
                } catch (Exception e) {
                    return j;
                }
                return null;
            }

            public int doIt() throws Exception {
                throw new Exception("FORGET IT");
            }
        }
        assertThat(new Test().main()).isEqualTo(1);
    }
}
