package com.edu.mock.crazyloops;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TrickySwitch {

    @Test
    void test01() {
        final List<String> list = new ArrayList<>();
        char i;
        LOOP:
        for (i = 0; i < 5; i++) {
            switch (i++) {
                case '0':
                    list.add("A");
                case 1:
                    list.add("B");
                    break LOOP;
                case 2:
                    list.add("C");
                    break;
                case 3:
                    list.add("D");
                    break;
                case 4:
                    list.add("E");
                case 'E':
                    list.add("F");
            }
        }

        assertThat(list).containsExactly("C", "E", "F");
    }

    @Test
    void test02() {
        LABEL:
        System.out.println("OK");
        for (; ; ) {
            break;
        }
    }

    @Test
    void test03() {
        final List<Integer> list = new ArrayList<>();
        switch (5) {
            default:
                list.add(-1);
            case 3:
                list.add(3);
            case 5:
                list.add(5);
            case 4:
                list.add(4);
        }

        assertThat(list).containsExactly(5, 4);
    }

    @Test
    void test04() {
        final List<Integer> list = new ArrayList<>();
        switch (3) {
            case 3:
                list.add(3);
            default:
                list.add(-1);
            case 5:
                list.add(5);
            case 4:
                list.add(4);
        }

        assertThat(list).containsExactly(3, -1, 5, 4);
    }

    @Test
    void test05() {
        final List<Integer> list = new ArrayList<>();
        switch (8) {
            case 3:
                list.add(3);
            default:
                list.add(-1);
            case 5:
                list.add(5);
                break;
            case 4:
                list.add(4);
        }

        assertThat(list).containsExactly(-1, 5);
    }

    @Test
    void test06() {
        var k = 0;
        for (int i = 0, j = 0; i < 1; i++) {
            k++;
        }
        assertThat(k).isEqualTo(1);
    }

    @Test
    void test07() {
        var k = 0;
        for (var i = 0; i < 5; i++) {
            k++;
        }
        assertThat(k).isEqualTo(5);
    }

    @Test
    void test08() {
        var list = new ArrayList<>();
        var args = new String[]{"java", "Test", "closed"};
        if (args[2].equals("open"))
            if (args[3000].equals("someone"))
                list.add("Hello!");
            else list.add("Go away " + args[1]);
        assertThat(list).isEmpty();
    }

    @Test
    void test09() {
        var c = 0;
        var flag = true;
        for (var i = 0; i < 3; i++) {
            while (flag) {
                c++;
                if (i > c || c > 5) flag = false;
            }
        }
        assertThat(c).isEqualTo(6);
    }


}
