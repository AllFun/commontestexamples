package com.edu.interfaces;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import java.time.LocalTime;

import static org.assertj.core.api.Assertions.assertThat;

class DefaultMethods {

    @Test
    void assertDefaultMethods() {
        assertThat(new AnyClass().invokePublic())
                .isLessThanOrEqualTo(LocalTime.now().getNano());
    }

}

interface AnyTestInterface {

    default int invokePublic() {
        return invodeDynPrivate();
    }

    private int invodeDynPrivate() {
        return invokeStatic();
    }

    static int invokeStatic() {
        return LocalTime.now().getNano();
    }
}

@RequiredArgsConstructor
class AnyClass implements AnyTestInterface {
}
