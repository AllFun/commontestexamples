package com.edu.interfaces;

import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class OverrideSameDefault {
    @Test
    void shouldOverride() {
        final Pet pet = new Pet();
        assertThat(pet.eat(10))
                .isEqualTo("overriden");
        assertThat(Dog.getDescription())
                .isEqualTo("Dog");
        assertThat(Cat.getDescription())
                .isEqualTo("Cat");
        assertThat(Cat.anotherStaticMethod())
                .isEqualTo("anotherStaticMethod");
    }
}

interface Dog {
    default String eat(final int measure) {
        return measure + "";
    }

    static String getDescription() {
        return "Dog";
    }
}

interface Cat {
    default String eat(final int measure) {
        return measure + "";
    }

    static String getDescription() {
        return "Cat";
    }

    static String anotherStaticMethod() {
        return "anotherStaticMethod";
    }
}

@RequiredArgsConstructor
class Pet implements Dog, Cat {
    @Override
    public String eat(int measure) {
        return "overriden";
    }
}