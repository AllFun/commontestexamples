package com.edu.var;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class TestVar {

    @Test
    void testVar() {
        var row = "Any_ROW";
        assertThat(row).isExactlyInstanceOf(String.class);
    }
}
