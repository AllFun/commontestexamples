package com.edu.interesting;

import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ClassInsideOfMethod {

    private String anyMethod(final int a, final int b, final int c) {
        @ToString
        @RequiredArgsConstructor
        class InnerClass {
            final int a, b, c;
        }

        return new InnerClass(a, b, c).toString();
    }

    @Test
    void shouldInitInnerOfMethodClass() {
        assertThat(anyMethod(1, 2, 3))
                .isEqualTo("InnerClass(a=1, b=2, c=3)");
    }
}
