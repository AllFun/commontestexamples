package com.edu.interesting;

import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import static lombok.AccessLevel.PRIVATE;
import static org.assertj.core.api.Assertions.assertThat;

public class AnonymousClass {

    @Test
    void shouldVerifyClasses() {
        assertThat(new TestClass().getName()).isEqualTo("default");
        assertThat(new TestClass() {
            @Override
            public String getName() {
                return "overriden";
            }
        }.getName())
                .isEqualTo("overriden");
    }

    @NoArgsConstructor(access = PRIVATE)
    class TestClass {
        public String getName() {
            return "default";
        }
    }
}
