package com.edu.interesting;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldNameConstants;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap.SimpleEntry;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class InstanceInitializerTest {

    @Test
    void shouldTest() {
        IntStream.rangeClosed(1, 10)
                .mapToObj(id -> new SimpleEntry(id, "name" + id))
                .forEach(simpleEntry ->
                        assertThat(new AnyImmutableClass((String) simpleEntry.getValue()))
                                .hasFieldOrPropertyWithValue(AnyImmutableClass.Fields.id, simpleEntry.getKey())
                                .hasFieldOrPropertyWithValue(AnyImmutableClass.Fields.name, simpleEntry.getValue()));
    }

}

@Getter
@FieldNameConstants
@RequiredArgsConstructor
class AnyImmutableClass {
    private static int maxId = 0;
    private final String name;
    private int id;

    {
        id = ++maxId;
    }
}