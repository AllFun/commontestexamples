package com.edu.interesting;

import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InstanceOfTest {
    @Test
    void shouldPass() {
        int count = 0;
        final D anyObject = new D();
        if (anyObject instanceof D) {
            count++;
        }
        if (anyObject instanceof C) {
            count++;
        }
        if (anyObject instanceof B) {
            count++;
        }
        if (anyObject instanceof A) {
            count++;
        }
        assertThat(count).isEqualTo(4);
    }
}

interface A {
}

interface B {
}

interface C extends B {
}

@NoArgsConstructor
class D implements A, C {
}