package com.edu.string;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class StringBuilderTest {

    @Test
    void test001() {
        final String s = "MINIMUM";
        assertThat(s.substring(4, 7)).isEqualTo("MUM");
        assertThat(s.substring(5)).isEqualTo("UM");
        assertThat(s.substring(s.indexOf('I', 3))).isEqualTo("IMUM");
        assertThatThrownBy(() -> s.substring(s.indexOf('I', 4)))
                .isExactlyInstanceOf(StringIndexOutOfBoundsException.class);
    }

    @Test
    void test002() {
        final StringBuilder sb = new StringBuilder("abc");
        assertThat(sb.toString()).isEqualTo("abc");

        sb.append("def");
        assertThat(sb.toString()).isEqualTo("abcdef");

        sb.append("defghijklm", 3, 6);
        assertThat(sb.toString()).isEqualTo("abcdefghi");

        sb.insert(0, ":");
        assertThat(sb.toString()).isEqualTo(":abcdefghi");

        sb.insert(sb.length(), "defghijklm", 6, 9);
        assertThat(sb.toString()).isEqualTo(":abcdefghijkl");
    }

    @Test
    void test003() {
        assertThat(new StringBuilder("0123456789").replace(2, 4, "xxxxxx").toString()).isEqualTo("01xxxxxx456789");
        assertThat(new StringBuilder("0123456789").replace(2, 2, "xxxxxx").toString()).isEqualTo("01xxxxxx23456789");
        assertThat(new StringBuilder("0123456789").replace(2, 3, "xxxxxx").toString()).isEqualTo("01xxxxxx3456789");
    }
}
