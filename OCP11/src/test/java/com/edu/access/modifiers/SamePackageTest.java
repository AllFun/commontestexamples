package com.edu.access.modifiers;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class SamePackageTest {

    private static final String base = "any";

    private String baseDyn = "baseDyn";

    @Test
    void test() {
        assertThat(ChildClass.child).isEqualTo("any_child");
        assertThat(new ChildClass().childDyn).isEqualTo("baseDyn_child");
    }

    private class ChildClass {

        private static final String child = base + "_child";

        private String childDyn = baseDyn + "_child";


    }
}
